$(function() {
    var gNumbers = {A: null, B: null};
    $('#startForm').submit(function() {
        var numA = parseInt($('#numberA').val(), 10);
        var numB = parseInt($('#numberB').val(), 10);
        if (!validateA(numA)) {
            return false;
        }
        gNumbers.A = numA;
        if (!validateB(numB)) {
            return false;
        }
        gNumbers.B = numB;
        restart();
        main();
        return false;
    });
    function restart() {
        $('.arrow').addClass('invisible');
        // чтобы исчезло нормально
        setTimeout(function() {
            $('.arrow').css({left: 35}); // 35 - начальное значение
        }, 1000);
        $('.i-input')
            .val(null)
            .removeClass('i-input--readonly')
            .attr('readonly', null);
        $('.answerInput').addClass('hidden');
        $('.question').removeClass('hidden');
    }
    function validateA(number) {
        if (!(number >= 6 && number <= 9)) {
            alert('number A not in valid range');
            return false;
        }
        return true;
    }
    function validateB(number) {
        if (!gNumbers.A) {
            alert('need set number A');
            return false;
        }
        var sum = gNumbers.A + number;
        if (!(sum >= 11 && sum <= 14)) {
            alert('number B not in valid range');
            return false;
        }
        return true;
    }
    // for test
    //gNumbers = {A: 6, B: 5};
    //main();
    //

    function main() {
        $('#headingA').html(gNumbers.A);
        $('#headingB').html(gNumbers.B);
        $('.main').removeClass('invisible');

        var successInputA = function() {
            renderArrow('B', 'A', allArrowsSuccess);
        };
        renderArrow('A', null, successInputA);
    }
    function allArrowsSuccess() {
        $('.heading .question').addClass('hidden');
        $('.heading .answerInput').removeClass('hidden');
        var sum = gNumbers.A + gNumbers.B;
        onInputEvent($('.heading .answerInput'), function() {
            if (validateInput(this, sum)) {
                // congratulations
                return true;
            }
        });
    }
    function renderArrow(numIdent, fromNumIdent, successInput) {
        // 12.5 и 39.15 - значения для стрелки, которая указывает на оси на 1
        var $arrow = $('#arrow' + numIdent);
        var number = gNumbers[numIdent];
        if (fromNumIdent) {
            var fromNum = gNumbers[fromNumIdent];
            var left = 39.15 * fromNum + parseInt($arrow.css('left'), 10);
            $arrow.css({left: left});
        }
        var height = 12.5 * number;
        var width = 39.15 * number;
        $arrow.height(height);
        $arrow.width(width);

        onInputEvent($arrow.find('.arrow_input'), function() {
            if (validateArrowInput(this, numIdent, number)) {
                successInput();
                return true;
            }
        });
        $arrow.removeClass('invisible');
    }
    function validateArrowInput(input, numIdent, expectedNum) {
        var headingSel = '#heading' + numIdent;
        if (!validateInput(input, expectedNum)) {
            $(headingSel).addClass('heading_number--error');
            return false;
        }
        $(headingSel).removeClass('heading_number--error');
        return true;
    }
    function validateInput(input, expectedNum) {
        var $input = $(input);
        if ($input.val() != expectedNum) {
            $input.addClass('i-input--error');
            return false;
        }
        $input.removeClass('i-input--error');
        $input.attr('readonly', true).html(expectedNum);
        $input.addClass('i-input--readonly');
        return true;
    }
    function onInputEvent($input, callback) {
        var success = false;
        $input.on('keyup', function(e) {
            var charValue = String.fromCharCode(e.keyCode);
            var valid = /^[0-9]+$/.test(charValue);
            if (!valid) return;
            if (success) return;
            success = callback.apply(this, null);
        });
    }
});
