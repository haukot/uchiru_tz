var gulp = require('gulp');
var haml = require('gulp-ruby-haml');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');

var opt = {
    finalFolder: 'priv/'
};


gulp.task('clean', function(cb) {
    del(['index.html', opt.finalFolder + '*'], cb);
});
gulp.task('haml', ['clean'], function() {
    return gulp.src(['index.haml'])
        .pipe(haml())
        .pipe(gulp.dest('./'));
});
gulp.task('sass', ['clean'], function () {
    return gulp.src(['assets/**/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest(opt.finalFolder));
});
gulp.task('dev', ['haml', 'sass'], function () {});
gulp.task('watch', function() {
    gulp.watch(['index.haml', 'assets/**/*.scss'], ['dev']);
});
